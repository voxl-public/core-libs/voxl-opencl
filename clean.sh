#!/bin/bash
#
# Modal AI Inc. 2021
# author: james@modalai.com



rm -rf ipk/control.tar.gz
rm -rf ipk/data/
rm -rf ipk/data.tar.gz
rm -rf *.ipk
rm -rf .bash_history